package dev.mrkevr.springcacheredis.person;

import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/persons")
@CacheConfig(cacheNames = "persons")
public class PersonController {

    private final PersonRepository personRepository;

    @GetMapping
    @Cacheable(keyGenerator = "personCacheKeyGenerator")
    public List<Person> getAll(){
        return personRepository.findAll();
    }

    @GetMapping("/{id}")
    @Cacheable(keyGenerator = "personCacheKeyGenerator")
    public Person getById(@PathVariable Long id){
        return personRepository.findById(id).orElseGet(null);
    }

}
