package dev.mrkevr.springcacheredis.config;

import dev.mrkevr.springcacheredis.person.Person;
import dev.mrkevr.springcacheredis.person.PersonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class AppConfig {

    private final PersonRepository personRepository;

    @Bean
    CommandLineRunner clr(){
        return args -> {
            personRepository.save(new Person("Jake"));
            personRepository.save(new Person("Amy"));
            personRepository.save(new Person("Charles"));
            };
    }

}
